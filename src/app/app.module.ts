import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {UsersService} from './users/users.service';
import {ProductsService} from './products/products.service';
import {PostsService} from './posts/posts.service';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostFormComponent } from './post-form/post-form.component';
import { TryLinkComponent } from './try-link/try-link.component';
import{AngularFireModule} from 'angularfire2';

import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';

export const firebaseConfig = {
    apiKey: "AIzaSyAjSE9ZWHfPPyLVDiU-Pi7erdw052ycvnU",
    authDomain: "fortest-2f135.firebaseapp.com",
    databaseURL: "https://fortest-2f135.firebaseio.com",
    storageBucket: "fortest-2f135.appspot.com",
    messagingSenderId: "298936585180"
  };

const appRoutes:Routes= [
  {path: 'users', component:UsersComponent},
  {path: 'posts', component:PostsComponent},
  {path: 'products', component:ProductsComponent},
  {path: '', component:UsersComponent},
  {path: '**', component:PageNotFoundComponent},
  ]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    UserFormComponent,
    PageNotFoundComponent,
    PostFormComponent,
    TryLinkComponent,
    ProductsComponent,
    ProductComponent,
    CategoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,PostsService,ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
