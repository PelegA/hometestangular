import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'my-users',
  templateUrl: './users.component.html',
  styles: [`
        .users li { cursor: default; }
        .users li:hover { background: #ecf0f1; }
        .list-group-item.active, 
        .list-group-item.active:hover { 
          background-color: #ecf0f1;
          border-color: #ecf0f1; 
          color: #2c3e50;
         }
   `]
 })
export class UsersComponent implements OnInit {
users;
currentUser;
isLoading:boolean=true;

  constructor(private _userService:UsersService) { }

  select(user){
		this.currentUser = user; 
 }
  ngOnInit() {
    //this.users= this._userService.getUsers();
    this._userService.getUsers().subscribe(usersData=>
      {
        this.users=usersData;
        this.isLoading=false;
        console.log(this.users);
      }); 
  }
  deleteUser(user){
    this._userService.deleteUser(user);
  }
  addUser(user){
    this._userService.addUser(user);
  }
  updateUser(user){
    this._userService.updateUser(user);
  }
}
