import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';
@Component({
  selector: 'my-products',
  templateUrl: './products.component.html',
  styles: [`
        .users li { cursor: default; }
        .users li:hover { background: #ecf0f1; }
        .list-group-item.active, 
        .list-group-item.active:hover { 
          background-color: #ecf0f1;
          border-color: #ecf0f1; 
          color: #2c3e50;
         }
   `]
 })
export class ProductsComponent implements OnInit {
  products;
  constructor(private _productService:ProductsService) { }
  
  deleteProduct(product){
    this._productService.deleteProduct(product);
  }
  ngOnInit() {
    this._productService.getProducts().subscribe(productsData=>
      {
        this.products=productsData;
      }); 
  }
}
