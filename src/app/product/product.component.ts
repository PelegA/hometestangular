import { Component, OnInit,EventEmitter, Output } from '@angular/core';
import {Product} from './product'
@Component({
  selector: 'my-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {
  //product:Product;
  @Output() deleteEvent=new EventEmitter<Product>();
  constructor() { }
  product:Product;
  sendDelete(){
    this.deleteEvent.emit(this.product);
  }
  ngOnInit() {
    
  }


}
